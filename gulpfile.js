var elixir = require('laravel-elixir');
require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
  mix.sass([
    'app.scss'
  ], 'resources/assets/css');

  mix.styles([
    'libs/bootstrap-3-3-5.min.css',
    'libs/font-awesome.css',
    'libs/dropzone-4-2-0.css',
    'app.css'
  ]);
});

elixir(function(mix) {
    mix.scripts([
        'libs/jquery-2.1.4.min.js',
        'libs/bootstrap-3-3-5.min.js',
        'libs/dropzone-4-2-0.js'
    ]);
});

elixir(function(mix) {
    mix.browserify(
      'main.js'
    );
});

//elixir(function(mix) {
//    mix.scriptsIn('public/js');
//});
