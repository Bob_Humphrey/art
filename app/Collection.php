<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $table = 'collections';

    public static function getCollectionName($id)
    {
        $collection = Collection::find($id);
        return $collection->collection_name;
    }
}
