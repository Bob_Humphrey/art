<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Site;
use App\Collection;

class SiteController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index', 'create', 'store', 'edit', 'update',
    'confirm', 'delete', 'destroy', 'editImage', 'updateImage']]);
    }


  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
      $sites = Site::orderBy('id', 'desc')->paginate(50);
      return view('sites.index')->with([
      'sites' => $sites,
    ]);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */

  public function create()
  {
      return view('sites.create');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
      $file = $request->file('file');
      $photoDirectory = env('BH_PHOTO_DIRECTORY');
      $fileExtension = $file->getClientOriginalExtension();
      $fileName =  str_random(10) . '.' . $fileExtension;
      $image = \Image::make($file->getRealPath());
      $image->widen(500);
      $image->save($photoDirectory . '/site_files/normal/'. $fileName);
      $image->widen(100);
      $image->save($photoDirectory . '/site_files/thumbnail/'. $fileName);
      $site = new Site;
      $site->image_file = $fileName;
      $site->save();
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
      $site = Site::find($id);
      $tagName = Tag::getTagName($site->tag_id);
      return view('sites.show')->with([
      'site' => $site,
      'tagName' => $tagName,
    ]);
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
      $site = Site::find($id);
      $tagList = Tag::getTagList();
      return view('sites.edit')->with([
      'site' => $site,
      'tagList' => $tagList,
    ]);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
      $site = Site::findOrFail($id);
      $site->site_name = $request->site_name;
      $site->url = $request->url;
      $site->tag_id = $request->tag_id;
      if ($request->has('favorite')) {
          $site->favorite = 1;
      } else {
          $site->favorite = 0;
      }
      $site->save();
      flashMessage("The site has been updated.", "alert-success");
      $url = url('sites');
      return redirect($url);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
      $site = Site::find($id);
      $site->delete();
      flashMessage("The site has been deleted.", "alert-success");
      $redirect = action('SiteController@index');
      return redirect($redirect);
  }

  /**
  * Confirm that the user wants to delete the site.
  */

  public function confirm($id)
  {
      $site = Site::find($id);
      $tagName = Tag::getTagName($site->tag_id);
      return view('sites.confirm')->with([
      'site' => $site,
      'tagName' => $tagName,
    ]);
  }

  /**
  * Display all sites in a particular collection.
  */

  public function collection($id)
  {
      $tags = Tag::getByCollection($id);
      $collectionName = Collection::getCollectionName($id);
      return view('sites.collection')->with([
      'tags' => $tags,
      'collectionName' => $collectionName,
    ]);
  }

  /**
  * Display all sites in a particular category.
  */

  public function tag($id, $tag)
  {
      $tagSites = Tag::getByTag($tag);
    //return $tag;
    return view('sites.tag')->with([
      'tag' => $tagSites[0],
    ]);
  }

    public function editImage($id)
    {
        $site = Site::find($id);
        $tagName = Tag::getTagName($site->tag_id);
        return view('sites.editImage')->with([
      'site' => $site,
      'tagName' => $tagName,
    ]);
    }

    public function updateImage(Request $request)
    {
        $file = $request->file('file');
        $id = $request->id;
        $photoDirectory = env('BH_PHOTO_DIRECTORY');
        $fileExtension = $file->getClientOriginalExtension();
        $fileName =  str_random(10) . '.' . $fileExtension;
        $image = \Image::make($file->getRealPath());
        $image->widen(500);
        $image->save($photoDirectory . '/site_files/normal/'. $fileName);
        $image->widen(100);
        $image->save($photoDirectory . '/site_files/thumbnail/'. $fileName);
        $site = Site::find($id);
        $site->image_file = $fileName;
        $site->save();
    }

    public function test()
    {
        $tags = Tag::with('sites')->where('collection', 1)->orderBy('name')->get();
        return $tags;
    }
}
