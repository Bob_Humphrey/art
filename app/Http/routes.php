<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/home', function () {
    return view('home');
});
Route::get('/tags', ['middleware' => 'auth', function () {
    return view('tags');
}]);

Route::get('/sites/collection/{id}', 'SiteController@collection');
Route::get('/sites/collection/{id}/{tag}', 'SiteController@tag');
Route::get('/sites/{id}/confirm', 'SiteController@confirm');
Route::get('/sites/{id}/editImage', 'SiteController@editImage');
Route::get('/sites/tag/{id}', 'SiteController@tag');
Route::get('/sites/test', 'SiteController@test');
Route::post('/sites/{id}/updateImage', 'SiteController@updateImage');
Route::resource('sites', 'SiteController');

Route::resource('api/sites', 'SiteApiController');
Route::resource('api/tags', 'TagApiController');

// Authentication routes…
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');
