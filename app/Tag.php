<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tag extends Model
{
    protected $table = 'tags';
    public $timestamps = false;

    public function sites()
    {
        return $this->hasMany('App\Site')->orderBy('site_name');
    }

    public static function getByCollection($id)
    {
        $tags = Tag::with('sites')->where('collection', $id)->orderBy('name')->get();
        return $tags;
    }

    public static function getTagLinksByCollection($id)
    {
        $tags = Tag::where('collection', $id)->orderBy('name')->get();
        return $tags;
    }

    public static function getByTag($id)
    {
        $tags = Tag::with('sites')->where('id', $id)->get();
        return $tags;
    }



    public static function getTagList()
    {
        $tagList = array();
        $tags = DB::select('
      select tags.id, tags.name, collections.collection_name
      from tags
      left join collections on
      tags.collection = collections.id
      order by collections.collection_name, tags.name');
        foreach ($tags as $tag) {
            $key = (string)$tag->id;
            $value = $tag->collection_name . ' - ' . $tag->name;
            $tagList[$key] = $value;
        }
        return $tagList;
    }

    public static function getTagName($id)
    {
        $tag = Tag::find($id);
        if (isset($tag->name)) {
            return $tag->name;
        } else {
            return '';
        }
    }
}
