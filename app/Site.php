<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'sites';
    protected $fillable = ['site_name', 'url', 'favorite', 'tag_id'];

    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }
}
