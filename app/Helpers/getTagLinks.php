<?php

use App\Tag;

function getTagLinks()
{
    $tagLinks = session('tagLinks');
    if ($tagLinks) {
        return $tagLinks;
    }

    $tagLinks = array();
    $artTags = Tag::getTagLinksByCollection(1);
    $photographyTags = Tag::getTagLinksByCollection(2);
    $tagLinks['artTags'] = $artTags->toArray();
    $tagLinks['photographyTags'] = $photographyTags->toArray();
    session(['tagLinks' => $tagLinks]);
    return $tagLinks;
}
