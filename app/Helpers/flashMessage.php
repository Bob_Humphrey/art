<?php

function flashMessage($message, $alertType)
{
    \Log::info($message);
    session()->flash('flash_message', $message);
    session()->flash('alert_type', $alertType);
}
