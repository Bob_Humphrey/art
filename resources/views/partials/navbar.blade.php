<?php

$tagLinks = getTagLinks();  // Helper
$artTags = $tagLinks['artTags'];
$photographyTags = $tagLinks['photographyTags'];

$homeLink = url('/home');
$artLink = url('/sites/collection/1');
$photographyLink = url('/sites/collection/2');
$tagsLink = url('/tags');
$addSiteLink = url('/sites/create');
$sitesLink = url('/sites');
$loginLink = action('Auth\AuthController@getLogin');
$logoutLink = action('Auth\AuthController@getLogout');

// Set the current page as active.
$activeArt = '';
$activePhotography = '';
$activeSubmissions = '';
$activeTags = '';
$activeAddSite = '';
$activeSites = '';
$activeAdmin = '';
$activeLogin = '';
$path = ($_SERVER['REQUEST_URI']);

$pathArt = '/sites/collection/1';
$pathPhotography = '/sites/collection/2';
$pathTags = '/tags';
$pathAddSite= '/sites/create';
$pathLogin = '/auth/login';
$pathSites = '/sites';

if (substr($path, 0, strlen($pathArt)) == $pathArt) {
    $activeArt = 'active';
} elseif (substr($path, 0, strlen($pathPhotography)) == $pathPhotography) {
    $activePhotography = 'active';
} elseif (substr($path, 0, strlen($pathTags)) == $pathTags) {
    $activeTags = 'active';
} elseif (substr($path, 0, strlen($pathAddSite)) == $pathAddSite) {
    $activeAddSite = 'active';
} elseif (substr($path, 0, strlen($pathSites)) == $pathSites) {
    $activeSites = 'active';
} elseif (substr($path, 0, strlen($pathLogin)) == $pathLogin) {
    $activeLogin = 'active';
}


?>

<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://bobhumphrey.org"><img src="/images/logo-80.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="{{$homeLink}}">
            <span class="nav-item visible-lg-inline">ART INSPIRATION</span>
          </a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown {{$activeArt}}">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"
          role="button" aria-haspopup="true" aria-expanded="false">
          ART <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{$artLink}}">ALL</a></li>
            <li role="separator" class="divider"></li>
            @foreach ($artTags as $tag)
            <?php $link = url('sites/collection/1/' . $tag['id']);
            $label = strtoupper($tag['name']); ?>
            <li><a href="{{$link}}">{{$label}}</a></li>
            @endforeach
          </ul>
        </li>
        <li class="dropdown {{$activePhotography}}">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"
          role="button" aria-haspopup="true" aria-expanded="false">
          PHOTOGRAPHY <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{$photographyLink}}">ALL</a></li>
            <li role="separator" class="divider"></li>
            @foreach ($photographyTags as $tag)
            <?php $link = url('sites/collection/2/' . $tag['id']);
            $label = strtoupper($tag['name']); ?>
            <li><a href="{{$link}}">{{$label}}</a></li>
            @endforeach
          </ul>
        </li>
        <li class="{{$activeTags}}"><a href="{{$tagsLink}}">TAGS</a></li>
        <li class="{{$activeAddSite}}"><a href="{{$addSiteLink}}">ADD SITE</a></li>
        <li class="{{$activeSites}}"><a href="{{$sitesLink}}">SITES</a></li>
        @if (Auth::check())
        <li><a href="{{$logoutLink}}">LOGOUT</a></li>
        @else
        <li class="{{$activeLogin}}"><a href="{{$loginLink}}">LOGIN</a></li>
        @endif
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
