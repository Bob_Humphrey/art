<?php
$homeLink = url('/home');
$artLink = url('/sites/collection/1');
$photographyLink = url('/sites/collection/2');
$tagsLink = url('/tags');
$addSiteLink = url('/sites/create');
$sitesLink = url('/sites');
?>

<div id="menu">
  <a href="{{$homeLink}}" type="button" class="btn btn-link" >Home</a>
  <a href="{{$artLink}}" type="button" class="btn btn-link" @click="menuTags">Art</a>
  <a href="{{$photographyLink}}" type="button" class="btn btn-link" @click="menuTags">Photography</a>
  <a href="{{$tagsLink}}" type="button" class="btn btn-link" @click="menuTags">Tags</a>
  <a href="{{$addSiteLink}}" type="button" class="btn btn-link" >Add Site</a>
  <a href="{{$sitesLink}}" type="button" class="btn btn-link" >Admin Sites</a>
</div>
