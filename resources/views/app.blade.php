<!DOCTYPE html>
<html>
<head class="backdrop">
  <title>Art Inspiration</title>
  <link rel="stylesheet" href="/css/all.css">
</head>
<body>
    @include('partials.navbar')
    <div id="app" class="container">
    @include('partials.alerts')
    @yield('content')
    </div>
    @include('partials.footer')
</body>
<script src="/js/all.js"></script>
<script src="/js/main.js"></script>
</html>
