<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
    </head>
    <body>
        <div class="container">

          <div id="admin">

            <form id="search">
              Search <input name="query" v-model="searchQuery">
            </form>

            <grid
              :data="tags"
              :columns="gridColumns"
              :filter-key="searchQuery">
            </grid>




          </div>

        </div>
    </body>
    <script src="js/main.js"></script>
</html>
