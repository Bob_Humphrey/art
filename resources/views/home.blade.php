@extends('app')

@section('content')

<div id="home-page">

  <h2>Art Inspiration</h2>

  <h3>Description</h3>

  <p>I use this application to collect links to my favorite artists' websites.
  Each artist included here creates work that I find beautiful,
  engaging, and inspirational.</p>

  <h3>Built With</h3>

  <p>PHP, Laravel, JavaScript, Vue.js, DropZone.js, MySQL, Bootstrap,
  and the Intervention image manipulation library.</p>

  <h3>To Use</h3>

  <p>Pick a category from the main menu.  Then click on any image to be
  taken to that artist's website.</p>

  <h3>Additional Admin Pages</h3>

  <p>Available only after logging in.</p>

  <div class="row">
    <div class='col-md-6'>
      <img src="images/list.jpg" class="img-responsive"/>
    </div>
    <div class='col-md-6'>
      <img src="images/update.jpg" class="img-responsive"/>
    </div>
  </div>

  <div class="row">
    <div class='col-md-6'>
      <img src="images/tags.jpg" class="img-responsive"/>
    </div>
  </div>

</div>

@stop
