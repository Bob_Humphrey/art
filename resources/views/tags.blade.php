@extends('app')

@section('content')

<div id="tag-delete-modal" class="hide" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" @click="dismissDelete" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirm delete</h4>
      </div>
      <div class="modal-body">
        <p>Do you really want to delete the tag with the name '@{{tag.name}}'?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" @click="dismissDelete">Cancel</button>
        <button type="button" class="btn btn-primary" @click="deleteTag">Delete Tag</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="tags-page">
  <h2>Tags</h2>
  <div class="row">

    <div class="col-md-4">
      <div id="tag-form" class="hide">

        <div class="alert alert-danger" v-if="showTagErrors">
          <ul>
            <li v-show="!tagValidation.collection">Collection is required.</li>
            <li v-show="!tagValidation.name">Name is required.</li>
          </ul>
        </div>

        <div class="alert alert-danger" transition="alert-msg" v-if="tagDBFail">There is a problem with the database.</div>

        <div class="alert alert-success" transition="alert-msg" v-if="tagSuccess">@{{tagSuccessMessage}}</div>

        <h3>@{{tagDetailFunction}} Tag</h3>

        <div class="form-group radio-group">
          <input type="radio" id="art" value="1" v-model="tag.collection">
          <label for="art">Art</label>
          <input type="radio" id="photography" value="2" v-model="tag.collection">
          <label for="photography">Photography</label>
          <br>
        </div>

        <div class="form-group">
          <label for="name">Name</label>
          <textarea class="form-control" id="name" placeholder="Name" v-model="tag.name"></textarea>
        </div>

        <button class="btn btn-primary" v-on:click="saveTag">@{{tagDetailFunction}}</button>

      </div>
    </div>

    <div class="col-md-8">
      <form id="search">
        Search <input name="query" v-model="tagSearch">
      </form><br />

      <table id="tag-table" class="table table-striped hide" >
        <thead>
          <tr>
            <th
              @click="sortByTags('id')"
              :class="{active: tagSortKey == 'id'}">
              ID
              <span class="arrow"
                :class="tagSortRows['id'] > 0 ? 'asc' : 'dsc'">
              </span>
            </th>
            <th
              @click="sortByTags('name')"
              :class="{active: tagSortKey == 'name'}">
              Name
              <span class="arrow"
                :class="tagSortRows['name'] > 0 ? 'asc' : 'dsc'">
              </span>
            </th>
            <th
              @click="sortByTags('collection_name')"
              :class="{active: tagSortKey == 'collection_name'}">
              Collection
              <span class="arrow"
                :class="tagSortRows['collection_name'] > 0 ? 'asc' : 'dsc'">
              </span>
            </th>
            <th>
              <i class="fa fa-pencil"></i>
            </th>
            <th>
              <i class="fa fa-close"></i>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="
            entry in tags
            | filterBy tagSearch
            | orderBy tagSortKey tagSortRows[tagSortKey]">
            <td>
              @{{entry.id}}
            </td>
            <td>
              @{{entry.name}}
            </td>
            <td>
              @{{entry.collection_name}}
            </td>
            <td>
              <button type="link" class="btn btn-link" @click="showUpdateTag(entry.id)"><i class="fa fa-pencil"></i></button>
            </td>
            <td>
              <button type="link" class="btn btn-link" @click="deleteTagConfirm(entry.id)"><i class="fa fa-close"></i></button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
</div>


</div>

@stop
