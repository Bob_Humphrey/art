<?php

?>

@extends('app')

@section('content')


<h2>Sites</h2>

<table class="table table-striped table-bordered table-condensed table-hover">
  <tr>
    <th>Image</a></th>
    <th>Name</th>
    <th><i class="fa fa-eye"></i></th>
    <th><i class="fa fa-pencil"></i></th>
    <th><i class="fa fa-pencil-square-o"></i></th>
    <th><i class="fa fa-close"></i></th>
  </tr>

@foreach ($sites as $site)
  <?php $editLink = action('SiteController@edit', ['id' => $site->id]);?>
  <?php $editImageLink = action('SiteController@editImage', ['id' => $site->id]);?>
  <?php $showLink = action('SiteController@show', ['id' => $site->id]);?>
  <?php $confirmLink = action('SiteController@confirm', ['id' => $site->id]);?>
  <tr>
    <td><a href="{{$site->url}}" target="_blank"><img src="site_files/thumbnail/{{$site->image_file}}"></a></td>
    <td>{{$site->site_name}}</td>
    <td><a href="{{$showLink}}"><i class="fa fa-eye"></i></a></td>
    <td><a href="{{$editLink}}"><i class="fa fa-pencil"></i></a></td>
    <td><a href="{{$editImageLink}}"><i class="fa fa-pencil-square-o"></i></a></td>
    <td><a href="{{$confirmLink}}"><i class="fa fa-close"></i></a></td>
  </tr>
@endforeach

</table>

{!! $sites->render() !!}


@stop
