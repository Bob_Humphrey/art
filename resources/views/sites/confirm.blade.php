<?php
$deleteLink = action('SiteController@destroy', ['id' => $site->id]);
$cancelLink = action('SiteController@show', ['id' => $site->id]);
$imageFile = url("site_files/normal/$site->image_file");
$favorite = showCheckMark($site->favorite);
?>

@extends('app')

@section('content')

<h2>Do you really want to delete this site?</h2>

<div id="site-show-page">
  <div class="row">
    <div class='col-md-4'>
      <img src="{{$imageFile}}" class="img-responsive"/>
    </div>

    <div class='col-md-8'>
      @include('sites.detail')

      <form id="sites-form" action="{{$deleteLink}}" method="post">
        <input type="hidden" name="_method" value="DELETE">
        <div class="form-group">
          <br />
            <button class="btn btn-primary" type="submit">Delete</button>
            <a class="btn btn-primary" href="{{$cancelLink}}" role="button">Cancel</a>
        </div>

      </div>
    </div>
  </form>
</div>



@stop
