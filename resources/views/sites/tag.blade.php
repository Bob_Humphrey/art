<?php
$sites = $tag->sites;
$tagName = ucwords($tag->name);

?>

@extends('app')

@section('content')

<h2>{{$tagName}}</h2>

<div id="sites-gallery-page">

  @include('sites.gallery')

</div>

@stop
