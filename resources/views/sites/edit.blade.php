<?php
$confirmLink = action('SiteController@confirm', ['id' => $site->id]);
$actionLink = url("/sites/$site->id");
$imageFile = url("site_files/normal/$site->image_file");
$siteName = $site->site_name;
$siteUrl = $site->url;
$favorite = '';
if ($site->favorite) {
    $favorite = 'checked';
}
$tag = (string)$site->tag_id;
$selected = '';
?>

@extends('app')

@section('content')

<h2>Update Site</h2>

<div id="sites-add-page">
  <div class="row">
    <div class='col-md-4'>
      <img src="{{$imageFile}}" class="img-responsive"/>
    </div>

    <div class='col-md-8'>
      <form id="sites-form" action="{{$actionLink}}" method="post">
        <input type="hidden" name="_method" value="PUT">

        <div class="form-group">
          <label for="site-name">Site Name</label>
          <input type="text" class="form-control" id="site_name" name="site_name" placeholder="Site Name" value="{{$siteName}}">
        </div>

        <div class="form-group">
          <label for="url">Url</label>
          <input type="text" class="form-control" id="url" name="url" placeholder="Url" value="{{$siteUrl}}">
        </div>

        <div class="row">
          <div class='col-md-6'>
            <div class="form-group">
              <label for="tag_id">Tag</label>
              <select class="form-control" name="tag_id">
                @foreach($tagList as $key => $value)
                  @if ($key == $tag)
                    <?php $selected = 'selected'; ?>
                  @else
                    <?php $selected = ''; ?>
                  @endif
                <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class='col-md-6'>
            <br />
            <div class="checkbox">
              <label>
                <input type="checkbox" name="favorite" {{$favorite}}>
                Favorite
              </label>
            </div>
          </div>
        </div>

        <div class="form-group">
          <br />
            <button class="btn btn-primary" type="submit">Update</button>
            <a class="btn btn-primary" href="{{$confirmLink}}" role="button">Delete</a>
        </div>

      </div>
    </div>
  </form>
</div>



@stop
