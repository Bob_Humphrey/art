@extends('app')

@section('content')

<h2>{{$collectionName}}</h2>

<div id="sites-gallery-page">

@foreach ($tags as $tag)

  <?php
  $sites = $tag->sites;
  $tagName = ucwords($tag->name);
  ?>
  <div class="sites-gallery-page-tag">
    <h3>{{$tagName}}</h3>

    @include('sites.gallery')
  </div>

@endforeach

</div>

@stop
