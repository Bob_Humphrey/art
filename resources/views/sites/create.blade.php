<?php
$sitesStoreUrl = url('sites');
?>

@extends('app')

@section('content')

<h2>Add Site</h2>

<form action="{{$sitesStoreUrl}}"
method="post"
class="dropzone"
id="site-dropzone"></form>

@stop
