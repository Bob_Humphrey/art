<?php
$editLink = action('SiteController@edit', ['id' => $site->id]);
$imageFile = url("site_files/normal/$site->image_file");
$favorite = showCheckMark($site->favorite)
?>

@extends('app')

@section('content')

<h2>{{$site->site_name}}</h2>

<div id="site-show-page">
  <div class="row">
    <div class='col-md-4'>
      <img src="{{$imageFile}}" class="img-responsive"/>
    </div>

    <div class='col-md-8'>

      @include('sites.detail')

      <form id="sites-form" action="{{$editLink}}" method="get">
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
          <br />
            <button class="btn btn-primary" type="submit">Update</button>
        </div>
      </div>
    </div>
  </form>
</div>

@stop
