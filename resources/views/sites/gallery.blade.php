<?php $x = 0; ?>

<div class="row">
  <div class='col-md-4'>

    @foreach ($sites as $site)
        @if ( ( $x % 3 ) === 0 )
          <div class="gallery-site">
            <?php $imageFile = url("site_files/normal/$site->image_file"); ?>
            <a href="{{$site->url}}" target="_blank">
              <img src="{{$imageFile}}" class="img-responsive"/>
            </a>
            <div class="gallery-site-info">
              @if (Auth::check())
              <?php $link = action('SiteController@edit', ['id' => $site->id]); ?>
              <a href="{{$link}}">{{$site->site_name}}</a>
              @else
              {{$site->site_name}}
              @endif
            </div>
          </div>
        @endif
        <?php $x++; ?>
    @endforeach

  </div>

  <?php $x = 0; ?>

  <div class='col-md-4'>

    @foreach ($sites as $site)
      @if ( ( $x % 3 ) === 1 )
        <div class="gallery-site">
          <?php $imageFile = url("site_files/normal/$site->image_file"); ?>
          <a href="{{$site->url}}" target="_blank">
            <img src="{{$imageFile}}" class="img-responsive"/>
          </a>
          <div class="gallery-site-info">
            @if (Auth::check())
            <?php $link = action('SiteController@edit', ['id' => $site->id]); ?>
            <a href="{{$link}}">{{$site->site_name}}</a>
            @else
            {{$site->site_name}}
            @endif
          </div>
        </div>
      @endif
      <?php $x++; ?>
    @endforeach

  </div>

  <?php $x = 0; ?>

  <div class='col-md-4'>

    @foreach ($sites as $site)
      @if ( ( $x % 3 ) === 2 )
        <div class="gallery-site">
          <?php $imageFile = url("site_files/normal/$site->image_file"); ?>
          <a href="{{$site->url}}" target="_blank">
            <img src="{{$imageFile}}" class="img-responsive"/>
          </a>
          <div class="gallery-site-info">
            @if (Auth::check())
            <?php $link = action('SiteController@edit', ['id' => $site->id]); ?>
            <a href="{{$link}}">{{$site->site_name}}</a>
            @else
            {{$site->site_name}}
            @endif
          </div>
        </div>
      @endif
      <?php $x++; ?>
    @endforeach

  </div>
</div>
