<?php
$editUrl = action('SiteController@updateImage', ['id' => $site->id]);
$imageFile = url("site_files/normal/$site->image_file");
$favorite = showCheckMark($site->favorite)
?>

@extends('app')

@section('content')

<h2>Update Image</h2>

<div id="site-edit-image-page">
  <div class="row">
    <div class='col-md-4'>
      <img src="{{$imageFile}}" class="img-responsive"/>
    </div>

    <div class='col-md-8'>

      @include('sites.detail')

      <form action="{{$editUrl}}"
      method="post"
      class="dropzone"
      id="site-dropzone">
      <input type="hidden" name="id" value="{{$site->id}}">
      </form>

    </div>
</div>

@stop
