var Vue = require('vue');
Vue.use(require('vue-resource'));

var admin = new Vue({
  el: '#app',

  data: {

    // SITES

    sites:[],

    // TAGS

    showTagAlert: false,
    tag: {
      id: 0,
      collection: '',
      name: ''
    },
    tagDBFail: false,
    tagDetailFunction: 'Add',
    tagGridColumns: ['id', 'name', 'collection_name'],
    tags: [],
    tagSearch: '',
    tagSortKey: '',
    tagSortRows: {
      id: 1,
      name: 1,
      collection_name: 1,
    },
    tagSuccess: false,
    tagSuccessMessage: '',
  },

  computed: {

    // TAGS

		tagValidation: function () {
			return {
				name: !!this.tag.name.trim(),
				collection: !!this.tag.collection
			}
		},

		tagIsValid: function () {
			let validation = this.tagValidation
			return Object.keys(validation).every(function (key) {
				return validation[key]
			})
		},

    showTagErrors: function() {
      return !this.tagIsValid && this.showTagAlert
    }
	},

  ready:  function () {
    console.log('ready');
    this.fetchTags();
  },

  methods: {

    // TAGS

    addTag: function() {
      // Validation fails.
      if (! this.tagIsValid) {
        this.showTagAlert = true
      }
      // Validation passes.
      else {
        this.showTagAlert = false
        this.$http.post('/api/tags',
        {
          collection: this.tag.collection,
          name: this.tag.name
        }).then((response) => {
          // API action success.
          self = this
          this.tagSuccess = true
          this.tagSuccessMessage = 'The tag has been added.'
          setTimeout(function () {
            self.tagSuccess = false
            self.tagSuccessMessage = ''
          }, 5000)
          this.tag.collection = ''
          this.tag.name = ''
          this.fetchTags()

        }, (response) => {
          // API action fail.
          self = this
          this.tagDBFail = true
          setTimeout(function () {
            self.tagDBFail = false
          }, 5000)
        })
      }
    },

    deleteTag: function() {
      window.scrollTo(0, 0);
      this.$http.delete('/api/tags/' + this.tag.id)
       .then((response) => {
         // API action success.
         self = this
         this.tagSuccess = true
         this.tagSuccessMessage = 'The tag has been deleted.'
         setTimeout(function () {
           self.tagSuccess = false
           self.tagSuccessMessage = ''
         }, 5000)
         this.tag.collection = ''
         this.tag.name = ''
         this.fetchTags();

       }, (response) => {
         // API action fail.
         self = this
         this.tagDBFail = true
         setTimeout(function () {
           self.tagDBFail = false
         }, 5000)
       })
     this.dismissDelete()
    },

    deleteTagConfirm: function(id) {
      this.fetchTag(id)
      document.getElementById("tag-delete-modal").className =
        document.getElementById("tag-delete-modal").className.replace(/\bhide\b/,'modal')
    },

    dismissDelete: function() {
      document.getElementById("tag-delete-modal").className =
        document.getElementById("tag-delete-modal").className.replace(/\bmodal\b/,'hide')
    },

    fetchTag: function(id) {
      this.$http.get('/api/tags/' + id)
        .then((response) => {
          // API action success.
          this.tag.id = id
          this.tag.name = response.data.name
          this.tag.collection = response.data.collection
        }, (response) => {
          // API action fail.
          self = this
          this.tagDBFail = true
          setTimeout(function () {
            self.tagDBFail = false
          }, 5000)
        })
    },

    fetchTags: function() {
      this.$http.get('/api/tags')
      .then((response) => {
        // API action success.
        this.tags = response.data
        // Don't show the grid and detail form until the data is available.
        let hasTagTable = document.getElementById("tag-table");
        if (hasTagTable) {
          document.getElementById("tag-table").className =
          document.getElementById("tag-table").className.replace(/\bhide\b/,'')
        }
        let hasTagForm = document.getElementById("tag-form");
        if (hasTagForm) {
          document.getElementById("tag-form").className =
          document.getElementById("tag-form").className.replace(/\bhide\b/,'')
        }
      }, (response) => {
        // API action fail.
        self = this
        this.tagDBFail = true
        setTimeout(function () {
          self.tagDBFail = false
        }, 5000)
      })
    },

    menuTags: function() {
      let tagSortRows = {}
      this.tagGridColumns.forEach(function (ckey) {
        tagSortRows[ckey] = 1
      })
      this.tagSortRows = tagSortRows
    },

    saveTag: function () {
      if (this.tagDetailFunction === 'Add') {
        this.addTag()
      }
      else if (this.tagDetailFunction === 'Update') {
        this.updateTag()
      }
    },

    showUpdateTag: function(id) {
      this.tagDetailFunction = 'Update'
      this.fetchTag(id)
      window.scrollTo(0, 0);
    },

    sortByTags: function (key) {
      this.tagSortKey = key
      let tagSortRows = this.tagSortRows
      tagSortRows[key] = tagSortRows[key] * -1
      this.tagSortRows = tagSortRows
    },

    updateTag: function() {
      // Validation fails.
      if (! this.tagIsValid) {
        this.showTagAlert = true
      }
      // Validation passes.
      else {
        this.showTagAlert = false
        this.$http.patch('/api/tags/' + this.tag.id,
        {
          collection: this.tag.collection,
          name: this.tag.name
        }).then((response) => {
          // API action success.
          self = this
          this.tagSuccess = true
          this.tagSuccessMessage = 'The tag has been updated.'
          setTimeout(function () {
            self.tagSuccess = false
            self.tagSuccessMessage = ''
          }, 5000)
          this.fetchTags()

        }, (response) => {
          // API action fail.
          self = this
          this.tagDBFail = true
          setTimeout(function () {
            self.tagDBFail = false
          }, 5000)
        })
      }
    },

  }


});
